package permissions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import org.hsqldb.lib.HsqlArrayHeap;

import permissions.db.PagingInfo;
//import permissions.db.PersonDbManager;
import permissions.db.RepositoryCatalog;
import permissions.db.catalogs.HsqlRepositoryCatalog;
import permissions.db.repos.HsqlAddressRepository;
import permissions.db.repos.HsqlPermissionRepository;
import permissions.db.repos.HsqlPersonRepository;
import permissions.db.repos.HsqlRoleRepository;
import permissions.db.repos.HsqlUserRepository;
import permissions.domain.Address;
import permissions.domain.Permission;
import permissions.domain.Person;
import permissions.domain.Role;
import permissions.domain.User;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

    	String url = "jdbc:hsqldb:hsql://localhost/workdb";
		try(Connection connection = DriverManager.getConnection(url)) 
		{
			
			
			HsqlPersonRepository repo = new HsqlPersonRepository(connection);
			Person person = new Person();
		    person.setName("Bolesław");
		    person.setSurname("Prus");
			repo.add(person);

			HsqlAddressRepository repoAdd = new HsqlAddressRepository(connection);
			Address address = new Address();
		    address.setCountry("Bułgaria");
		    address.setCity("Baryton");
		    address.setStreet("Boleslawa Prusa");
		    address.setPostalCode ("33-111");
		    address.setHouseNumber("6");
		    address.setLocalNumber("6");
			repoAdd.add(address);
			
			HsqlPermissionRepository repMiss = new HsqlPermissionRepository(connection);
			Permission permission = new Permission();
		    permission.setName("Mozesz chyba");
			repMiss.add(permission);
			
			HsqlRoleRepository repRo = new HsqlRoleRepository(connection);
			Role role = new Role();
		    role.setName("Marchwiarz nizszego rzedu");
			repRo.add(role);

			HsqlUserRepository repUs = new HsqlUserRepository(connection);
			User user= new User();
		    user.setUsername("MiłosnikMarchewki3000");
		    user.setPassword("marchew");
			repUs.add(user);
			
	    	
		}
		
		 catch (SQLException e) {
			e.printStackTrace();
		}
    }
}
